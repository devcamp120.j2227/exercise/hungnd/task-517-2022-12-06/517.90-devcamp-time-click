import { Component } from "react";

class TimeClick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: []
        }
    }
    onBtnTimeClick = () => {
        const timeCurrent = this.getTime(new Date());
        this.setState({
            time: [...this.state.time, timeCurrent]
        })
    }
    getTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        var strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
        return strTime;
    }
    render() {
        const timeList = this.state.time;
        return (
            <div className="main">
                <div>
                    <ul>
                        <p>List:</p>
                        {timeList.map((data, index) => {
                            return(
                                <li key={index}> {index + 1}.  {data} </li>
                            )
                        })}
                    </ul>
                </div>
                <div>
                    <button onClick={this.onBtnTimeClick}>Add To List</button>
                </div>

            </div>
        )
    }
}
export default TimeClick;